package httpjson;


import java.lang.*;

public class Motorcycle {
    private final int Year;
    private final String Make;
    private final String Model;
    private final int Size;


    public Motorcycle(int Year, String Make, String Model, int Size) {
        this.Year = Year;
        this.Make = Make;
        this.Model = Model;
        this.Size = Size;
    }


    public String toString() {
        return "Year: " + Year + System.getProperty("line.separator") + "Make: " + Make + System.getProperty("line.separator") + "Model: " + Model + System.getProperty("line.separator") + "Engine Displacement: " + Size;
    }

}
