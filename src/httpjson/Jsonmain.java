package httpjson;

import java.net.*;
import java.io.*;
import java.util.*;
import java.lang.*;



//Adapted from Example code to work
public class Jsonmain {
    public static void main(String[] args) {


        Server.serverStart();
        System.out.println(Jsonmain.getContent("http://localhost:8080"));

        Map<Integer, List<String>> map = Jsonmain.getHeaders("http://localhost:8080");


        for (Map.Entry<Integer, List<String>> entry : map.entrySet()) {
            try {
                System.out.println("Key=" + entry.getKey() + entry.getValue());

            } catch (Exception e) {
              System.err.println(e.toString());
            }
        }
    }

    public static String getContent(String string) {

        String content = "";

        try {
            URL url1 = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url1.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();

            String l;
            while ((l = reader.readLine()) != null) {
                stringBuilder.append(l + "\n");
            }
            content = stringBuilder.toString();

        } catch (IOException e) {
            System.err.println(e.toString());
        }

        return content;
    }

    public static Map getHeaders(String string) {
        Map map1 = null;

        try {
            URL url2 = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url2.openConnection();

            map1 = http.getHeaderFields();

        } catch (IOException e) {
            System.err.println(e.toString());
        }


        return map1;

    }


}

