package httpjson;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;

import java.io.*;

import java.net.*;


public class Server {


    public static void main(String[] args) {

        serverStart();


    }

    public static void serverStart() {
        try {

//adapted from "https://www.logicbig.com/tutorials/core-java-tutorial/http-server/http-server-basic.html"
            HttpServer server = HttpServer.create(new InetSocketAddress(8080), 0);
            HttpContext context = server.createContext("/");
            context.setHandler(Server::handleRequest);
           // context.setHandler(q -> Server.handleRequest(q));// Just playing with Lambdas
            server.start();


        } catch (Exception e) {
        System.out.println(e.toString());
        }
    }

    private static void handleRequest(HttpExchange exchange) throws IOException {
        Motorcycle m = new Motorcycle(2018, "Honda", "CB300F", 300);
        String response = objectToJSON(m);
        exchange.sendResponseHeaders(200, response.getBytes().length);//response code and length
        OutputStream os = exchange.getResponseBody();
        os.write(response.getBytes());
        os.close();
    }


    public static String objectToJSON(Motorcycle motorcycle) {
        ObjectMapper map = new ObjectMapper();
        map.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);

        String mcJson = "";
        try {
            mcJson = map.writeValueAsString(motorcycle);

        } catch (JsonProcessingException e) {
            System.out.println(e.toString());
        }

        return mcJson;
    }


}

